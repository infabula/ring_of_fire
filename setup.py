from setuptools import setup, find_packages


setup(
    name='ringoffire',
    version='0.0.1',
    url='https://infabula.nl',
    author='Jeroen Meijer',
    author_email='jeroen@infabula.nl',
    packages=['ringoffire']
)
