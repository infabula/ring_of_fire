from machine import Pin, I2C
import network
from neopixel import NeoPixel
from mpu9250 import MPU9250
import time
import math
import urequests as requests
import credentials
import ujson


NB_OF_PIXELS = 30
HOME = "185.95.13.75"
PORT = 5000

pin = Pin(2, Pin.OUT)   # set GPIO0 to output to drive NeoPixels
np = NeoPixel(pin, 30)
wlan = network.WLAN(network.STA_IF)

# accelerometer
i2c = I2C(scl=Pin(33), sda=Pin(32))
sensor = MPU9250(i2c)


def get_pitch_roll(sensor):
    x = sensor.acceleration[0]
    y = sensor.acceleration[1]
    z = sensor.acceleration[2]
    
    pitch = 180.0 * math.atan2(x, math.sqrt(y * y + z*z))/ math.pi
    roll = 180 * math.atan2( y,  math.sqrt(x*x + z*z) )/ math.pi
    yaw = 180 * math.atan2(z, math.sqrt(x*x + z*z))/ math.pi   
    return pitch, roll, yaw

def pixil_round():
    while True:
        for index in range(30):
            np[index] = (6 * index, 0, 0)
            np.write()
            time.sleep(0.05)
            np[index] = (0, 0, 0)
            np.write()
        
def all_pixels_off(np):
    for index in range(NB_OF_PIXELS):
        np[index] = (0, 0, 0)
    np.write()
    
def all_pixels_on(np, color=(100, 100, 100)):
    for index in range(NB_OF_PIXELS):
        np[index] = color
    np.write()        
        
def flash_connecting(np):
    #connect_pin.on()
    all_pixels_on(np)
    print("on")
    time.sleep(0.5) 
    #connect_pin.value(0)
    all_pixels_off(np)
    print('off')
    time.sleep(0.5)
    
def flash_connected(np):
    #connect_pin.on()
    all_pixels_off(np)
    for index in range(NB_OF_PIXELS):
        np[index] = (0, 100, 0)
        time.sleep(0.05)
        np.write()
    all_pixels_off(np)

def send_request(np):
    url = "http://" + HOME + ":5000/api/device/0/pitchroll"
    print(url)
    pitch, roll, yaw = get_pitch_roll(sensor)
    
    try:
        payload = {"pitch": pitch, "roll": roll, "yaw": yaw}
        header = { "content-type": 'application/json; charset=utf-8'}
        
        response = requests.post(url=url, data=ujson.dumps(payload),
                                 headers=header)
        data = response.json()
        print(data)
        if 'lightmode' in data:
            if data['lightmode'] == "off":
                all_pixels_off(np)
            elif data['lightmode'] == "on":
                color = data.get('color', (100, 100, 100))
                all_pixels_on(np, color)
                
    except Exception as e:
        print("Wrong")
        print(e)


def wifi_connect(np):
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(credentials.essid, credentials.password)
        while not wlan.isconnected():
            flash_connecting(np)
    flash_connected(np)
    print('network config:', wlan.ifconfig())
    return wlan

def main():
    wlan = wifi_connect(np)
    while wlan.isconnected():
        send_request(np)
        time.sleep(2.0)
    
main()