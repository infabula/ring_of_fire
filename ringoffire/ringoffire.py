import datetime
from flask import Flask
from flask import render_template, url_for, request
from flask_restful import Resource, Api, reqparse, abort

app = Flask(__name__)
api = Api(app)

db = {'devices': {
                   0: { 'name': 'woody',
                        'active': False,
                        'lightmode': 'off',
                        'motion':[]
                      }
                 }
     }

## Web ##
@app.route('/', methods=['GET', 'POST'])
def index():
    data = {
        "name": "Ring of fire",
        "links": ["https://infabula.nl", "https://practice.infabula.nl"]
    }
    
    if request.method == "GET":
        print("It's a GET")
        return render_template('index.html', **data)
    elif request.method == "POST":
        print("It's a POST")        

        light_status = request.form['light'].strip()
        print("Light status:", light_status)

        device = db['devices'][0]

        if light_status == 'on':
            device['lightmode'] = 'on'
            data['name'] = "Swiched to fire"
        elif light_status == 'off':
            device['lightmode'] = 'off'
            data['name'] == "Switched to dark"
            
        return render_template('index.html', **data)


## REST API ##
class PitchRoll(Resource):
    def post(self, device_id):
        
        parser = reqparse.RequestParser()
        parser.add_argument('pitch', type=float, help='pitch of the device')
        parser.add_argument('roll', type=float, help='roll of the device')        
        args = parser.parse_args()
        print("Device", device_id)
        print("pitch", args['pitch'])
        print("roll", args['roll'])
        try:
            device = db['devices'][device_id]
            device['active'] = True
            pr_data = { #'timestamp': datetime.DateTime.now(),
                        'pitch': args['pitch'],
                        'roll': args['roll']
            }
            device['motion'].append(pr_data)
            return {'lightmode': device['lightmode'] }, 201
        except Exception as e:
            print("Could not store motion data")
            abort(404, message="device {} doesn't exist".format(device_id))
        
    def get(self, device_id):
        if not device_id in db['devices']:
             abort(404, message="device {} doesn't exist".format(device_id))
        # get the latest 20 motions
        motion = db['devices'][device_id]['motion'][-10:]
        return motion, 200


api.add_resource(PitchRoll, '/api/device/<int:device_id>/pitchroll', endpoint='device_ep')

             
class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

api.add_resource(HelloWorld, '/api/')



def main():
    app.run(debug=True, port=5000, host="0.0.0.0")

if __name__ == '__main__':
    main()
